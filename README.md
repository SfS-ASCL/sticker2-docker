This repository hosts Docker images of sticker2 models. For
more information, see:

* https://github.com/stickeritis/sticker2/
* https://github.com/stickeritis/sticker2-models/